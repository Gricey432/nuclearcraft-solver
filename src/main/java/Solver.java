import java.util.*;

@SuppressWarnings("WeakerAccess")
public class Solver implements Runnable {

    enum Block {
        CELL(0),
        MODERATOR(0),
        WATER(60),
        REDSTONE(90),
        QUARTZ(70),
        GOLD(120),
        GLOWSTONE(130),
        LAPIS(120),
        DIAMOND(150),
        LIQUID_HELIUM(140),
        ENDERIUM(120),
        CRYOTHEUM(160),
        IRON(80),
        EMERALD(160),
        COPPER(80),
        TIN(100),
        MAGNESIUM(110),
        ;

        int coolingRate;
        Block(int coolingRate) {
            this.coolingRate = coolingRate;
        }
    }

    class Layout {
        /**
         * Simulates a 2D array in a single list
         * e.g. the indices of a 3x3 would be
         * 0 1 2
         * 3 4 5
         * 6 7 8
         */
        int width;
        int height;
        Block[] blocks;  // blocks[width * y + x]

        Layout(int width, int height) {
            this(width, height, new Block[width * height]);
        }

        private Layout(int width, int height, Block[] blocks) {
            this.width = width;
            this.height = height;
            this.blocks = blocks;
        }

        private int coordsToIndex(int x, int y) {
            return this.width * y + x;
        }

        Block get(int x, int y) {
            return blocks[coordsToIndex(x, y)];
        }

        void set(int x, int y, Block value) {
            blocks[coordsToIndex(x, y)] = value;
        }
        
        int getCoolingRate() {
            int total = 0;
            for (Block block : blocks) {
                if (block != null) {
                    total += block.coolingRate;
                }
            }
            return total;
        }

        Layout copy() {
            return new Layout(width, height, blocks.clone());
        }
    }

    LinkedList<Layout> layoutQueue;
    Layout currentWinner;
    Integer currentWinnerScore = 0;

    public static void main(String[] args) {
        Solver solver = new Solver();
        solver.run();
    }

    Solver() {
        this.layoutQueue = new LinkedList<>();
    }

    @Override
    public void run() {
        // Doesn't work smaller than 3
        int width = 3;
        int height = 3;

        int layoutsConsidered = 0;

        // Assumes moderator blocks are active
        Layout startingLayout = new Layout(width, height);
        startingLayout.set(1, 1, Block.CELL);  // Set the centre to a cell
        layoutQueue.add(startingLayout);

        // Coord deltas for neighbours
        // List of [dx, dy]
        int[][] neighbourDeltas = new int[][]{
            {-1, 0},
            {1, 0},
            {0, -1},
            {0, 1},
        };

        while (!layoutQueue.isEmpty()) {
            Layout layout = layoutQueue.remove();
            boolean spawnedChild = false; // Track whether this layout was a leaf node
            layoutsConsidered += 1;

            // For each empty cell, create a new layout with each possible
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (layout.get(x, y) == null) {
                        Set<Block> validBlocks = new HashSet<>();
                        int[] neighbourCounts = new int[Block.values().length];
                        int neighbourCasingCount = 0;

                        // Iterate neighbours and add counts
                        for (int[] d : neighbourDeltas) {
                            // d = {dx, dy}
                            int tx = x + d[0];
                            int ty = y + d[1];
                            if (tx >= 0 && tx < width && ty >= 0 && ty < height) {
                                Block neighbourType = layout.get(tx, ty);
                                if (neighbourType != null) {
                                    neighbourCounts[neighbourType.ordinal()] += 1;
                                }
                            } else {
                                // Casing
                                neighbourCasingCount += 1;
                            }
                        }


                        // Water Cooler - Must touch at least one Reactor Cell or active moderator block.
                        if (neighbourCounts[Block.CELL.ordinal()] >= 1 || neighbourCounts[Block.MODERATOR.ordinal()] >= 1) {
                            validBlocks.add(Block.WATER);
                        }

                        // Redstone Cooler - Must touch at least one Reactor Cell.
                        if (neighbourCounts[Block.CELL.ordinal()] >= 1) {
                            validBlocks.add(Block.REDSTONE);
                        }

                        // Quartz Cooler - Must touch at least one active moderator block.
                        if (neighbourCounts[Block.MODERATOR.ordinal()] >= 1) {
                            validBlocks.add(Block.QUARTZ);
                        }

                        // Gold Cooler - Must touch at least one active Water Cooler and one active Redstone Cooler.
                        if (neighbourCounts[Block.WATER.ordinal()] >= 1 && neighbourCounts[Block.REDSTONE.ordinal()] >= 1) {
                            validBlocks.add(Block.GOLD);
                        }

                        // Glowstone Cooler - Must touch at least two active moderator blocks.
                        if (neighbourCounts[Block.MODERATOR.ordinal()] >= 2) {
                            validBlocks.add(Block.GLOWSTONE);
                        }

                        // Lapis Cooler - Must touch at least one Reactor Cell and one Reactor Casing.
                        if (neighbourCounts[Block.CELL.ordinal()] >= 1 && neighbourCasingCount >= 1) {
                            validBlocks.add(Block.LAPIS);
                        }

                        // Diamond Cooler - Must touch at least two active Water Coolers and one active Quartz Cooler.
                        if (neighbourCounts[Block.WATER.ordinal()] >= 2 && neighbourCounts[Block.QUARTZ.ordinal()] >= 1) {
                            validBlocks.add(Block.DIAMOND);
                        }

                        // Liquid Helium Cooler - Must touch exactly one active Redstone Cooler and at least one Reactor Casing.
                        if (neighbourCounts[Block.REDSTONE.ordinal()] == 1 && neighbourCasingCount >= 1) {
                            validBlocks.add(Block.LIQUID_HELIUM);
                        }

                        // Enderium Cooler* - Must touch exactly three Reactor Casings at exactly one vertex.
                        // Can cheat here because we don't allow "pancake reactors"
                        if (neighbourCasingCount == 3) {
                            validBlocks.add(Block.ENDERIUM);
                        }

                        // Cryotheum Cooler* - Must touch at least two Reactor Cells.
                        if (neighbourCounts[Block.CELL.ordinal()] >= 2) {
                            validBlocks.add(Block.CRYOTHEUM);
                        }

                        // Iron Cooler - Must touch at least one active Gold Cooler.
                        if (neighbourCounts[Block.GOLD.ordinal()] >= 1) {
                            validBlocks.add(Block.IRON);
                        }

                        // Emerald Cooler - Must touch at least one active moderator block and one Reactor Cell.
                        if (neighbourCounts[Block.MODERATOR.ordinal()] >= 1 && neighbourCounts[Block.CELL.ordinal()] >= 1) {
                            validBlocks.add(Block.EMERALD);
                        }

                        // Copper Cooler - Must touch at least one active Glowstone Cooler.
                        if (neighbourCounts[Block.GLOWSTONE.ordinal()] >= 1) {
                            validBlocks.add(Block.COPPER);
                        }

                        // Tin Cooler - Must be at least between two active Lapis Coolers along the same axis.
                        // Only do complicated check if the minimum check passes
                        if (neighbourCounts[Block.LAPIS.ordinal()] >= 2) {
                            if ((0 < x && x < width - 1 && layout.get(x-1, y) == Block.LAPIS && layout.get(x+1, y) == Block.LAPIS)
                                    || (0 < y && y < height - 1 && layout.get(x, y-1) == Block.LAPIS && layout.get(x, y+1) == Block.LAPIS)) {
                                validBlocks.add(Block.TIN);
                            }
                        }

                        // Magnesium Cooler - Must touch at least one Reactor Casing and one active moderator block.
                        if (neighbourCasingCount >= 1 && neighbourCounts[Block.MODERATOR.ordinal()] >= 1) {
                            validBlocks.add(Block.MAGNESIUM);
                        }


                        // Generate a new layout for each valid block
                        if (validBlocks.size() > 0) {
                            spawnedChild = true;
                            for (Block newBlock : validBlocks) {
                                Layout newLayout = layout.copy();
                                newLayout.set(x, y, newBlock);
                                layoutQueue.addFirst(newLayout);
                            }
                        }
                    }
                }
            }

            // If the layout made no children, it is complete
            if (!spawnedChild) {
                // The layout is complete, no more blocks can be added
                // Calculate the total heat absorption and compare to the reigning champ
                int score = layout.getCoolingRate();
                if (score > currentWinnerScore) {
                    currentWinner = layout;
                    currentWinnerScore = score;
                }
            }
        }

        System.out.printf("Total layouts considered: %d\n", layoutsConsidered);
        System.out.printf("Best layout cooling power: %d\n", currentWinnerScore);
    }
}
