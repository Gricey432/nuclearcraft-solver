import gurobi.*;

import java.util.*;

@SuppressWarnings("WeakerAccess")
public class GurobiSolver {
    enum Block {
        CELL(0),
        MODERATOR(0),
        WATER(60),
        REDSTONE(90),
        QUARTZ(70),
        GOLD(120),
        GLOWSTONE(130),
        LAPIS(120),
        DIAMOND(150),
        LIQUID_HELIUM(140),
        ENDERIUM(120),
        CRYOTHEUM(160),
        IRON(80),
        EMERALD(160),
        COPPER(80),
        TIN(100),
        MAGNESIUM(110),
        ;

        int coolingRate;
        Block(int coolingRate) {
            this.coolingRate = coolingRate;
        }
    }

    int width; // x
    int depth; // y
    int height; // z
    int nCubes;

    // Helper for neighbours
    int[][] neighbourDeltas = new int[][]{
            // x, y, z
            {-1, 0, 0},
            {0, -1, 0},
            {0, 0, -1},
            {0, 0, 1},
            {0, 1, 0},
            {1, 0, 0},
    };

    public static void main(String[] args) {
        int size = 5;
        GurobiSolver app = new GurobiSolver(size, size, size);
        try {
            app.run();
        } catch (GRBException e) {
            System.out.println("Error code: " + e.getErrorCode() + ". " + e.getMessage());
            System.exit(1);
        }
    }

    GurobiSolver(int width, int depth, int height) {
        this.width = width;
        this.depth = depth;
        this.height = height;
        this.nCubes = width * depth * height;
    }

    int coordsToIndex(int x, int y, int z) {
        // Indexing starts in bottom back left corner
        // Moves along x, y, then z
        return x + width * (y + depth * z);
    }

    int[] indexToCoords(int index) {
        int z = index / (width * depth);
        index -= (z * width * depth);
        int y = index / width;
        int x = index % width;
        return new int[]{ x, y, z };
    }

    void addNeighbourConstraint(
            GRBModel model, GRBVar thisCellTypeVar, List<EnumMap<Block, GRBVar>> neighbours, Block[] types,
            double minCount, String name
    ) throws GRBException {
        GRBLinExpr lhs = new GRBLinExpr();
        for (EnumMap<Block, GRBVar> neighbour : neighbours) {
            for (Block block : types) {
                lhs.addTerm(1.0, neighbour.get(block));
            }
        }
        GRBLinExpr rhs = new GRBLinExpr();
        rhs.addTerm(minCount, thisCellTypeVar);
        model.addConstr(lhs, GRB.GREATER_EQUAL, rhs, name);
    }

    public void run() throws GRBException {
        GRBEnv grbEnv = new GRBEnv("reactor.log");
        GRBModel model = new GRBModel(grbEnv);

        // Create variables
        List<EnumMap<Block, GRBVar>> cubes = new ArrayList<>();
        for (int i = 0; i < nCubes; i++) {
            EnumMap<Block, GRBVar> map = new EnumMap<>(Block.class);
            for (Block blockType : Block.values()) {
                String name = String.format("%d_%s", i, blockType.toString());
                GRBVar var = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, name);
                map.put(blockType, var);
            }
            cubes.add(map);
        }

        // Objective is to maximise the cooling rate
        GRBLinExpr objectiveExpr = new GRBLinExpr();
        for (EnumMap<Block, GRBVar> map : cubes) {
            for (Map.Entry<Block, GRBVar> entry : map.entrySet()) {
                double coeff = entry.getKey().coolingRate;
                objectiveExpr.addTerm(coeff, entry.getValue());
            }
        }
        model.setObjective(objectiveExpr, GRB.MAXIMIZE);

        // Constraints
        // Only allow 0 or one choice per location (0 for air)
        // Sum cubes[i] <= 1.0
        for (int i = 0; i < nCubes; i++) {
            EnumMap<Block, GRBVar> map = cubes.get(i);
            GRBLinExpr expr = new GRBLinExpr();
            for (GRBVar var : map.values()) {
                expr.addTerm(1.0, var);
            }
            model.addConstr(expr, GRB.LESS_EQUAL, 1.0, "c_single_" + i);
        }

        // Fix the corners to be enderium
        // This is the best choice is most cases and cuts down solve time
        int [][] cornerCoords = new int[][] {
                {0, 0, 0},
                {0, 0, height-1},
                {0, depth-1, 0},
                {0, depth-1, height-1},
                {width-1, 0, 0},
                {width-1, 0, height-1},
                {width-1, depth-1, 0},
                {width-1, depth-1, height-1},
        };
        for (int[] coords : cornerCoords) {
            int i = coordsToIndex(coords[0], coords[1], coords[2]);
            EnumMap<Block, GRBVar> map = cubes.get(i);
            GRBVar var = map.get(Block.ENDERIUM);
            model.addConstr(1.0, GRB.EQUAL, var, "c_enderium_force_" + i);
        }

        // Cell type logic constraints
        for (int i = 0; i < nCubes; i++) {
            EnumMap<Block, GRBVar> map = cubes.get(i);
            int[] coords = indexToCoords(i);
            int x = coords[0];
            int y = coords[1];
            int z = coords[2];

            // Get neighbours
            List<EnumMap<Block, GRBVar>> neighbours = new ArrayList<>();
            int neighbourCasingCount = 0;
            for (int[] d : neighbourDeltas) {
                // d = {dx, dy, dz}
                int tx = x + d[0];
                int ty = y + d[1];
                int tz = z + d[2];
                if (tx >= 0 && tx < width && ty >= 0 && ty < depth && tz >= 0 && tz < height) {
                    int neighbourI = coordsToIndex(tx, ty, tz);
                    neighbours.add(cubes.get(neighbourI));
                } else {
                    // Casing
                    neighbourCasingCount += 1;
                }
            }

            // Block moderators, it places them in invalid positions cause I haven't put the constraints in yet
            model.addConstr(map.get(Block.MODERATOR), GRB.EQUAL, 0.0, "c_moderator_" + i);

            // Water Cooler - Must touch at least one Reactor Cell or active moderator block.
            addNeighbourConstraint(model, map.get(Block.WATER), neighbours, new Block[]{Block.CELL, Block.MODERATOR}, 1.0, "c_water_" + i);

            // Redstone Cooler - Must touch at least one Reactor Cell.
            addNeighbourConstraint(model, map.get(Block.REDSTONE), neighbours, new Block[]{Block.CELL}, 1.0, "c_redstone_" + i);

            // Quartz Cooler - Must touch at least one active moderator block.
            addNeighbourConstraint(model, map.get(Block.QUARTZ), neighbours, new Block[]{Block.MODERATOR}, 1.0, "c_quartz_" + i);

            // Gold Cooler - Must touch at least one active Water Cooler and one active Redstone Cooler.
            addNeighbourConstraint(model, map.get(Block.GOLD), neighbours, new Block[]{Block.WATER}, 1.0, "c_gold_water_" + i);
            addNeighbourConstraint(model, map.get(Block.GOLD), neighbours, new Block[]{Block.REDSTONE}, 1.0, "c_gold_water_" + i);

            // Glowstone Cooler - Must touch at least two active moderator blocks.
            addNeighbourConstraint(model, map.get(Block.GLOWSTONE), neighbours, new Block[]{Block.MODERATOR}, 2.0, "c_glowstone_" + i);

            // Lapis Cooler - Must touch at least one Reactor Cell and one Reactor Casing.
            if (neighbourCasingCount > 0) {
                addNeighbourConstraint(model, map.get(Block.LAPIS), neighbours, new Block[]{Block.CELL}, 1.0, "c_lapis_" + i);
            } else {
                // Force lapis to be 0
                model.addConstr(map.get(Block.LAPIS), GRB.EQUAL, 0.0, "c_lapis_" + i);
            }

            // Diamond Cooler - Must touch at least two active Water Coolers and one active Quartz Cooler.
            addNeighbourConstraint(model, map.get(Block.DIAMOND), neighbours, new Block[]{Block.WATER}, 2.0, "c_diamond_water_" + i);
            addNeighbourConstraint(model, map.get(Block.DIAMOND), neighbours, new Block[]{Block.QUARTZ}, 1.0, "c_diamond_quartz_" + i);

            // Liquid Helium Cooler - Must touch exactly one active Redstone Cooler and at least one Reactor Casing.
            if (neighbourCasingCount > 0) {
                // EXACTLY, can't reuse function
                // be careful here not to constrain the actual number of redstone neighbours
                // TODO
                // constrained to 0 to disable for now
                model.addConstr(map.get(Block.LIQUID_HELIUM), GRB.EQUAL, 0.0, "c_helium_" + i);
            } else {
                // Force liquid helium to be 0
                model.addConstr(map.get(Block.LIQUID_HELIUM), GRB.EQUAL, 0.0, "c_helium_" + i);
            }

            // Enderium Cooler - Must touch exactly three Reactor Casings at exactly one vertex.
            // Only works because we rely on this not being a pancake reactor (min 3x3x3)
            // Constrain to 0 if condition not met
            if (neighbourCasingCount != 3) {
                model.addConstr(map.get(Block.ENDERIUM), GRB.EQUAL, 0.0, "c_enderium_" + i);
            } // else unconstrained

            // Cryotheum Cooler - Must touch at least two Reactor Cells.
            addNeighbourConstraint(model, map.get(Block.CRYOTHEUM), neighbours, new Block[]{Block.CELL}, 2.0, "c_cryotheum_" + i);

            // Iron Cooler - Must touch at least one active Gold Cooler.
            addNeighbourConstraint(model, map.get(Block.IRON), neighbours, new Block[]{Block.GOLD}, 1.0, "c_iron_" + i);

            // Emerald Cooler - Must touch at least one active moderator block and one Reactor Cell.
            addNeighbourConstraint(model, map.get(Block.EMERALD), neighbours, new Block[]{Block.MODERATOR}, 1.0, "c_emerald_moderator_" + i);
            addNeighbourConstraint(model, map.get(Block.EMERALD), neighbours, new Block[]{Block.CELL}, 1.0, "c_emerald_cell_" + i);

            // Copper Cooler - Must touch at least one active Glowstone Cooler.
            addNeighbourConstraint(model, map.get(Block.COPPER), neighbours, new Block[]{Block.GLOWSTONE}, 1.0, "c_copper_" + i);

            // Tin Cooler - Must be at least between two active Lapis Coolers along the same axis.
            {
                // x
                GRBVar xVar = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "tin_lapis_x_" + i);
                if (x > 0 && x < width - 1) {
                    GRBVar var1 = cubes.get(coordsToIndex(x - 1, y, z)).get(Block.LAPIS);
                    GRBVar var2 = cubes.get(coordsToIndex(x + 1, y, z)).get(Block.LAPIS);
                    model.addGenConstrAnd(xVar, new GRBVar[]{var1, var2}, "c_tin_lapis_x_" + i);
                } else {
                    model.addConstr(xVar, GRB.EQUAL, 0.0, "c_tin_lapis_x_" + i);
                }

                // y
                GRBVar yVar = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "tin_lapis_y_" + i);
                if (y > 0 && y < depth - 1) {
                    GRBVar var1 = cubes.get(coordsToIndex(x, y - 1, z)).get(Block.LAPIS);
                    GRBVar var2 = cubes.get(coordsToIndex(x, y + 1, z)).get(Block.LAPIS);
                    model.addGenConstrAnd(yVar, new GRBVar[]{var1, var2}, "c_tin_lapis_y_" + i);
                } else {
                    model.addConstr(yVar, GRB.EQUAL, 0.0, "c_tin_lapis_y_" + i);
                }

                // z
                GRBVar zVar = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "tin_lapis_z_" + i);
                if (z > 0 && z < height - 1) {
                    GRBVar var1 = cubes.get(coordsToIndex(x, y, z - 1)).get(Block.LAPIS);
                    GRBVar var2 = cubes.get(coordsToIndex(x, y, z + 1)).get(Block.LAPIS);
                    model.addGenConstrAnd(zVar, new GRBVar[]{var1, var2}, "c_tin_lapis_z_" + i);
                } else {
                    model.addConstr(zVar, GRB.EQUAL, 0.0, "c_tin_lapis_z_" + i);
                }

                // OR them all
                GRBVar orVar = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "tin_lapis_or_" + i);
                model.addGenConstrOr(orVar, new GRBVar[]{xVar, yVar, zVar}, "c_tin_lapis_or_" + i);

                // Tin cooler may be present if or is true
                GRBLinExpr expr = new GRBLinExpr();
                expr.addTerm(0.0, orVar);
                GRBLinExpr rhs = new GRBLinExpr();
                rhs.addTerm(1.0, map.get(Block.TIN));
                model.addConstr(expr, GRB.GREATER_EQUAL, rhs, "c_tin_lapis_" + i);
            }

            // Magnesium Cooler - Must touch at least one Reactor Casing and one active moderator block.
            if (neighbourCasingCount > 0) {
                addNeighbourConstraint(model, map.get(Block.MAGNESIUM), neighbours, new Block[]{Block.MODERATOR}, 1.0, "c_magnesium_" + i);
            } else {
                // Force magnesium to be 0
                model.addConstr(map.get(Block.MAGNESIUM), GRB.EQUAL, 0.0, "c_magnesium_" + i);
            }
        }

        // Optimise
        model.set(GRB.DoubleParam.MIPGap, 0.03);
        model.optimize();

        // Print result
        {
            // Calculate longest type
            int longestTypeName = 0;
            for (Block block : Block.values()) {
                longestTypeName = Math.max(longestTypeName, block.name().length());
            }

            System.out.println("Cooling capacity: " + model.get(GRB.DoubleAttr.ObjVal));
            for (int i = 0; i < nCubes; i++) {
                // Print layer
                if (i % (width * depth) == 0) {
                    int layer = i / (width * depth);
                    System.out.println("Layer " + layer);
                }

                // Print cell contents
                EnumMap<Block, GRBVar> map = cubes.get(i);
                String name = "(air)";
                for (Map.Entry<Block, GRBVar> entry : map.entrySet()) {
                    Block block = entry.getKey();
                    GRBVar var = entry.getValue();
                    double result = var.get(GRB.DoubleAttr.X);
                    if (result > 0) {
                        name = block.name();
                    }
                }
                System.out.printf("%1$-" + (longestTypeName + 1) + "s", name); // Space pad

                // Newline for row end
                if (i % width == width - 1) {
                    System.out.print("\n");
                }
            }
        }

        // Dispose of model and environment
        model.dispose();
        grbEnv.dispose();
    }
}
